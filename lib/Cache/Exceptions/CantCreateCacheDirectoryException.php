<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Cache\Exceptions;


final class CantCreateCacheDirectoryException extends  AbstractCacheException
{

    public function __construct($dirName)
    {
        parent::__construct("Can't create cache directory: " . $dirName);
    }
}
