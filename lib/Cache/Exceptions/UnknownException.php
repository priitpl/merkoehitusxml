<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Cache\Exceptions;


final class UnknownException extends AbstractCacheException
{
    public function __construct(\Exception $e)
    {
        parent::__construct($e->getMessage(), $e->getCode());
    }
}
