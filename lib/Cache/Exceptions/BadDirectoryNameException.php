<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Cache\Exceptions;


final class BadDirectoryNameException extends AbstractCacheException
{
    public function __construct($directoryName)
    {
        parent::__construct("Bad directory name: '" . $directoryName . "'. Only characters, digits and low dashes are allowed.");
    }
}
