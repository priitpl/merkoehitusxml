<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Cache;


abstract class AbstractCache
{

    /**
     * Default time to live in seconds
     */
    const DEFAULT_TIME_TO_LIVE = 300;


    /**
     * @var int
     */
    protected $timeToLive = self::DEFAULT_TIME_TO_LIVE;


    /**
     * Read from cache.
     *
     * @param $key
     * @return mixed
     */
    abstract public function read($key);


    /**
     * Write to cache.
     *
     * @param $key
     * @param $data
     * @return mixed
     */
    abstract public function write($key, $data);


    /**
     * @return int
     */
    public function getTimeToLive()
    {
        return $this->timeToLive;
    }


    /**
     * Set time to live in seconds
     * @param int $timeToLive
     */
    public function setTimeToLive($timeToLive)
    {
        $this->timeToLive = $timeToLive;
    }
}
