<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Cache;


use lib\Cache\Exceptions\BadDirectoryNameException;
use lib\Cache\Exceptions\CacheDirNotDefinedException;
use lib\Cache\Exceptions\CantCreateCacheDirectoryException;
use lib\Cache\Exceptions\DirectoryNotWritableException;
use lib\Cache\Exceptions\FileNotExistsException;
use lib\Cache\Exceptions\ForbiddenCachePathException;
use lib\Cache\Exceptions\NotADirectoryException;
use lib\Reader\FileReader;

class FileCache extends AbstractCache
{

    /**
     * Cache absolute path.
     * @var string
     */
    protected $cacheDir;


    /**
     * Root directory.
     * Needed for checking, if user cache dir in permited area.
     *
     * @var string
     */
    protected $rootDir;


    /**
     * @param null $cacheDir
     * @throws BadDirectoryNameException
     * @throws DirectoryNotWritableException
     * @throws FileNotExistsException
     * @throws ForbiddenCachePathException
     * @throws NotADirectoryException
     */
    public function __construct($cacheDir = null)
    {
        // for now let it be this way.
        foreach(array('PWD', 'DOCUMENT_ROOT', 'HOME') as $key){
            if (isset($_SERVER[$key])) {
                $this->rootDir = $_SERVER[$key];
                break;
            }
        }

        // If cache directory is given, set it.
        if ($cacheDir) {
            $this->setCacheDir($cacheDir);
        }
    }

    /**
     * Get cache directory.
     * @return null
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }


    /**
     * Set cache directory.
     *
     * @param $cacheDir
     * @throws BadDirectoryNameException
     * @throws DirectoryNotWritableException
     * @throws FileNotExistsException
     * @throws ForbiddenCachePathException
     * @throws NotADirectoryException
     */
    public function setCacheDir($cacheDir)
    {
        $cacheDir = $this->sanitizePath($cacheDir);

        if (!$cacheDir) {
            throw new BadDirectoryNameException($cacheDir);
        }

        if (!preg_match('/[\w\d_]{1,}/', $cacheDir)){
            throw new BadDirectoryNameException($cacheDir);
        }

        if (strpos($cacheDir, $this->rootDir) !== 0){
            throw new ForbiddenCachePathException;
        }

        if (!file_exists($cacheDir)) {

            try{
                mkdir($cacheDir, 0775, true);
                chmod($cacheDir, 0775);

            } catch (\Exception $e){
                throw new CantCreateCacheDirectoryException($cacheDir);

            }
        }

        if (!file_exists($cacheDir)) {
            throw new FileNotExistsException($cacheDir);
        }

        if (!is_dir($cacheDir)) {
            throw new NotADirectoryException($cacheDir);
        }

        if (!is_writable($cacheDir)) {
            throw new DirectoryNotWritableException($cacheDir);
        }

        $this->cacheDir = $cacheDir;
    }

    /**
     * Read from cache.
     *
     * @param $key
     * @return string
     * @throws \Exception
     */
    public function read($key)
    {
        if(!$this->cacheDir){
            throw new CacheDirNotDefinedException;
        }

        $file = $this->getLatestFile($key);

        if (!$file) {
            return null;
        }
        $fileReader = new FileReader($this->cacheDir . DIRECTORY_SEPARATOR . $file);

        $data = $fileReader->read();

        if (!$data) {
            return null;
        }

        return $data;
    }


    /**
     * Write to cache.
     *
     * @param $key
     * @param $data
     * @throws CacheDirNotDefinedException
     */
    public function write($key, $data)
    {
        if (!$this->cacheDir) {
            throw new CacheDirNotDefinedException;
        }

        $dateTime = new \DateTime();

        $filePath = $this->generateFilePath($key);
        $fp = fopen($filePath, 'w');
        fwrite($fp, $data);
        fclose($fp);
    }


    /**
     * Get the latest cache.
     *
     * @param $key
     * @return null
     * @throws CacheDirNotDefinedException
     */
    protected function getLatestFile($key)
    {
        // throw exception, if cache dir not set
        if (!$this->cacheDir) {
            throw new CacheDirNotDefinedException;
        }

        $files = $this->getFilesHistory($key, true);
        if (!count($files)) {
            return null;
        }

        return $files[0];
    }


    /**
     * Get files history for given key.
     *
     * @param $key
     * @param bool|true $latestOnTop
     * @return array
     */
    protected function getFilesHistory($key, $latestOnTop = true)
    {
        $filePrefix = $this->generateFileName($key, false);

        $files = [];

        $dir = dir($this->cacheDir);

        $now = new \DateTime();
        $now = $now->getTimestamp();

        while($file = $dir->read())
        {
            // skip for certain files
            if (in_array($file, ['.', '..'])){
                continue;
            }

            // skip, if file prefix is not searchable
            if (strpos($file, $filePrefix) !== 0) {
                continue;
            }

            // Get file creation time
            $fileTime = substr($file, strrpos($file, '.') + 1);

            // remove file, if file is too old.
            if ($now - $fileTime - $this->getTimeToLive() > 0) {
                unlink($this->cacheDir . DIRECTORY_SEPARATOR . $file);
                continue;
            }

            // add file into history list.
            $files[] = $file;
        }

        // sort files depending on filter.
        usort($files, function($a = null, $b = null) use ($latestOnTop) {
            $aTime = substr($a, strrpos($a, '.') + 1);
            $bTime = substr($b, strrpos($b, '.') + 1);

            if ($aTime == $bTime) return 0;

            $latestOnTop = (int)$latestOnTop ? 1 : -1;
            return $aTime < $bTime ? 1 * $latestOnTop : -1 * $latestOnTop;
        });

        return $files;
    }


    /**
     * Generate absolute cache path for given key.
     *
     * @param $key
     * @return string
     */
    protected function generateFilePath($key)
    {
        $fileName = $this->generateFileName($key);

        return $this->cacheDir . DIRECTORY_SEPARATOR . $fileName;
    }


    /**
     * Generate cache file name for given key.
     *
     * @param $key
     * @param bool|true $withCacheSufix
     * @return string
     */
    protected function generateFileName($key, $withCacheSufix = true)
    {
        $dateTime = new \DateTime();
        return $fileName = $this->generateKeyFileName($key) . ($withCacheSufix ? '.cache.' . $dateTime->getTimestamp() : '');
    }


    /**
     * Get all cached files for given key.
     *
     * @todo - implementation required
     * @param $key
     * @return array
     */
    protected function getAllCacheFiles($key)
    {
        return [];
    }

    /**
     * Generate cache file filename.
     *
     * @param $key
     * @return mixed
     */
    protected function generateKeyFileName($key)
    {
        return preg_replace('/[.]{2,}/', '.', preg_replace('/[^a-zA-Z\d_\.]{1,}/', '', $key));
    }


    /**
     * Simple solution for path sanitization.
     * Do not sanitize hard paths with spaces
     * and UTFx encodings.
     *
     * @param $path
     * @return bool|string
     */
    public function sanitizePath($path)
    {
        $structure = [];
        $fromRoot = false;

        foreach(explode('/', $path) as $index => $part)
        {
            if ($index == 0 && !$part) {
                $fromRoot = true;
                continue;
            }


            // Skip for empty string.
            if ($part == '') {
                continue;
            }

            // Skip for current dir
            if ($part == ".") {
                continue;
            }

            // Return back
            if ($part == '..') {

                // If at the root
                if (count($structure) == 0) {
                    return false;
                }

                // move back in structure
                array_pop($structure);
                continue;
            }


            $structure[] = $part;
        }
        $sanitizedPath = join(DIRECTORY_SEPARATOR, $structure);

        if ($fromRoot) {
            $sanitizedPath = DIRECTORY_SEPARATOR . $sanitizedPath;
        }

        return $sanitizedPath;
    }

}
