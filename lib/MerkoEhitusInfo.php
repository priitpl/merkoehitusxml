<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib;


use lib\Cache\AbstractCache;
use lib\Cache\FileCache;
use lib\Http\Request;
use lib\Reader\Exceptions\BadRequestHostException;
use lib\Reader\Exceptions\RequestException;
use lib\Reader\HttpContentReader\Curl;
use lib\Reader\HttpReader;

class MerkoEhitusInfo
{

    /**
     * Shortcut for DIRECTORY_SEPARATOR
     */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * Xml source URL.
     *
     * @var string
     */
    protected $sourceUrl = 'http://hiirepadi.ee/merko.xml';

    /**
     * Cache instance. By default FileCache is used.
     * But possible to implement different types of cache.
     * @var AbstractCache
     */
    protected $cacheInstance;


    /**
     * Models root directory.
     * @var string
     */
    protected $modelsDirectory;


    /**
     * Store errors for user.
     * @todo - implement
     *
     * @var array
     */
    protected $errors = [];


    /**
     * Main constructor.
     * We set default caching mechanism and defining cache directory.
     */
    public function __construct()
    {

        // Set caching mechanism. We use FileCache.
        $this->cacheInstance = new FileCache(
            dirname(__FILE__) . self::DS . '..' . self::DS . 'cache' . self::DS . 'xml' . self::DS
        );

        $this->modelsDirectory = join(self::DS, [dirname(__FILE__), 'Model']) . self::DS;

    }


    /**
     * Get MerkoEhitus Orders.
     * Returns array of \lib\Model\TseMessage\Instruments\Orders\Order
     */
    public function getOrders()
    {
        $data = $this->fetchData();

        $xmlParser = new \lib\Parser\XmlParser();

        $modelsPath = $this->modelsDirectory . 'TseMessage' . self::DS
            . 'Instruments' . self::DS . 'Orders' . self::DS;

        $modelsNs   = 'lib\\Model\\TseMessage\\Instruments\\Orders';

        $results = $xmlParser->parse($data, '/TseMessage/Instruments/Orders', $modelsPath, $modelsNs);
        return $results;
    }


    /**
     * Get MerkoEhitus Indexes.
     * Returns array of \lib\Model\TseMessage\Indices\Index
     */
    public function getIndexes()
    {
        $data = $this->fetchData();

        $xmlParser = new \lib\Parser\XmlParser();

        $modelsPath = $this->modelsDirectory . 'TseMessage' . self::DS . 'Indices' . self::DS;
        $modelsNs   = 'lib\\Model\\TseMessage\\Indices';

        $results = $xmlParser->parse($data, '/TseMessage/Indices', $modelsPath, $modelsNs);
        return $results;
    }


    /**
     * Fetch latest data.
     * Caches data in defined caching mechanism
     * and if data is not cached or is expired(in case if expired caching mechanism
     * deletes old files by himself)
     *
     * @return string
     * @throws \Exception
     */
    protected function fetchData()
    {
        // Prepare request
        $request = new Request($this->sourceUrl, Request::GET, [], 5);

        $data = null;

        // Read from cache
        try{
            $data = $this->cacheInstance->read($this->sourceUrl);

        }
        // return false on bad request host/resource, continue on other
        catch(BadRequestHostException $e){
            $this->errors[] = 'Bad request resource';
            return false;

        }

        // Load from the web, if data not cached
        if ($data === null) {

            $httpReader = new HttpReader(new Curl(), $request);
            $data = $httpReader->read();

            // Write to cache new data
            $this->cacheInstance->write($this->sourceUrl, $data);

        }

        return $data;
    }
}
