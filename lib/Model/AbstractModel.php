<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Model;


use lib\Interfaces\SimpleXmlDataInjectorInterface;
use lib\Traits\ClazzTrait;
use lib\Traits\SimpleXmlDataInjectorTrait;

abstract class AbstractModel implements SimpleXmlDataInjectorInterface
{
    use ClazzTrait;

    use SimpleXmlDataInjectorTrait;
}
