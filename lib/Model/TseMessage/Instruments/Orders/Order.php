<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Model\TseMessage\Instruments\Orders;

use lib\Model\AbstractModel;

class Order extends AbstractModel
{
    /**
     * @var
     */
    protected $direction;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var int
     */
    protected $priorityTimestamp;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var float
     */
    protected $price;


    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return int
     */
    public function getPriorityTimestamp()
    {
        return $this->priorityTimestamp;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
}