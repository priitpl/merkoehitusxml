<?php

namespace lib\Model\TseMessage;
use lib\Model\AbstractModel;
use lib\Model\Instrument\Background;
use lib\Model\Instrument\Orders\Order;
use lib\Model\Instrument\Snapshot;
use lib\Model\Instrument\Trade;


/**
 * Created by PhpStorm.
 * User: priitpl
 */
class Instruments extends AbstractModel implements \Serializable
{
    /**
     * @var Background
     */
    protected $background;

    /**
     * @var Snapshot
     */
    protected $snapshot;

    /**
     * @var Trade[]
     */
    protected $trades;

    /**
     * @var Order[]
     */
    protected $orders;


    /**
     * @inheritdoc
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize([
            'background' => $this->background,
            'snapshot'  => $this->snapshot,
            'trades'    => $this->trades,
            'orders'    => $this->orders
        ]);
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function unserialize($serialized)
    {
        try{
            $data = unserialize($serialized);
            $this->background = $data['background'];
            $this->snapshot = $data['snapshot'];
            $this->trades   = $data['trades'];

        } catch(\Exception $e){

        }
    }

    /**
     * @return Background
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param $background
     * @return $this
     */
    public function setBackground($background)
    {
        $this->background = $background;
        return $this;
    }

    /**
     * @return Snapshot
     */
    public function getSnapshot()
    {
        return $this->snapshot;
    }

    /**
     * @param $snapshot
     * @return $this
     */
    public function setSnapshot($snapshot)
    {
        $this->snapshot = $snapshot;
        return $this;
    }

    /**
     * @return Instrument\Trade[]
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * @param $trades
     * @return $this
     */
    public function setTrades($trades)
    {
        $this->trades = $trades;
        return $this;
    }

    /**
     * @return Instrument\Orders\Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }


    /**
     * @param $orders
     * @return $this
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }


    /**
     * Add new order.
     * @param Order $order
     * @return $this
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;
        return $this;
    }




}
