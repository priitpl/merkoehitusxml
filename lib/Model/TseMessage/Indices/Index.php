<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Model\TseMessage\Indices;


use lib\Model\AbstractModel;

class Index extends AbstractModel
{

    /**
     * @attribute(name="symbol")
     * @var string
     */
    protected $symbol;

    /**
     * @var string
     */
    protected $fullName;

    /**
     * @var float
     */
    protected $lastValue;

    /**
     * @var \DateTime
     */
    protected $lastValueTime;

    /**
     * @var float
     */
    protected $percentageChange;

    /**
     * @var float
     */
    protected $changeYearBeginning;

    /**
     * @var float
     */
    protected $change12Month;

    /**
     * @var float
     */
    protected $previousClose;



    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return float
     */
    public function getLastValue()
    {
        return $this->lastValue;
    }

    /**
     * @return \DateTime
     */
    public function getLastValueTime()
    {
        return $this->lastValueTime;
    }

    /**
     * @return float
     */
    public function getPercentageChange()
    {
        return $this->percentageChange;
    }

    /**
     * @return float
     */
    public function getChangeYearBeginning()
    {
        return $this->changeYearBeginning;
    }

    /**
     * @return float
     */
    public function getChange12Month()
    {
        return $this->change12Month;
    }

    /**
     * @return float
     */
    public function getPreviousClose()
    {
        return $this->previousClose;
    }
}
