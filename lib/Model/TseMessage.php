<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Model;


use lib\Model\TseMessage\Indices;
use lib\Model\TseMessage\Instruments;

class TseMessage extends AbstractModel
{

    /**
     * @var \lib\Model\TseMessage\Instruments
     */
    protected $instruments;

    /**
     * @var \lib\Model\TseMessage\Instruments\News\News
     */
    protected $news;

    /**
     * @var \lib\Model\TseMessage\Indices
     */
    protected $indices;


    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'instruments'   => $this->instruments,
            'news'          => $this->news,
            'indices'       => $this->indices
        ];
    }

    /**
     * @param $data
     */
    public function unserialize($data)
    {

    }
}
