<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Traits;


trait SimpleXmlDataInjectorTrait
{

    public function injectData($data)
    {

        $reflection = new \ReflectionObject($this);

        $refProperties = $reflection->getProperties();

        $properties = [];

        /** @var  $property \ReflectionProperty*/
        foreach($refProperties as $property)
        {
            $proertyType = 'string';
            $comments = $property->getDocComment();

            $matches = array();

            if (preg_match('/@var[\s]{1,}(.{1,})/', $comments, $matches))
            {
                $var = $matches[1];

                $proertyType = $var;
//                if (in_array($var, ['string', 'float', 'number', 'int', 'integer', 'decimal', 'bool', 'boolean', 'biginteger', 'bigint'])){
//                    $proertyType = $var;
//                } else {
//                    $proertyType = $var;
//                }

            }


            $properties[$property->getName()] = $proertyType;
        }

        foreach($data as $key => $value)
        {
            // Attributes.
            if (strpos($key, '@') === 0) {
                $key = substr($key, 1);
            }

            // Convert to camelCase
            $fieldName = lcfirst($key);

            // skip, if property doesn't exists
            if (!array_key_exists($fieldName, $properties)) {
                continue;
            }


            $value = strlen((string)$value) == 0 ? null : (string)$value;


            if (is_array($properties[$fieldName])) {
                continue;
            }

            switch($properties[$fieldName]){
                /**
                 * For now just convert to integer
                 * But there is lib's that allow to create biginteger
                 * variables for Math.
                 */
                case 'bigint':
                case 'biginteger':

                // numbers
                case 'int':
                case 'integer':
                case 'number':
                    $this->$fieldName = (int)$value;
                    break;

                case 'float':
                case 'decimal':
                    $this->$fieldName = floatval($value);
                    break;

                case 'bool':
                case 'boolean':
                    $this->$fieldName = boolval($value);
                    break;

                case '\\DateTime':
                    $this->$fieldName = new \DateTime($value);
                    break;

                case 'string':
                default :
                    $this->$fieldName = $value;

            }
            // set property value.

        }

        return $this;
    }
}
