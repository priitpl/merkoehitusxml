<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Traits;


trait ClazzTrait
{
    static public function clazz()
    {
        return get_called_class();
    }
}
