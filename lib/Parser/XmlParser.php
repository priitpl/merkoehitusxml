<?php

namespace lib\Parser;
use lib\Cache\AbstractCache;
use lib\Http\Request;
use lib\Interfaces\SimpleXmlDataInjectorInterface;
use lib\Model\TseMessage;
use lib\Reader\HttpContentReader\Curl;
use lib\Reader\HttpReader;

/**
 * Created by PhpStorm.
 * User: priitpl
 */
class XmlParser
{

    /**
     * Parse given XML document(string).
     * Xpath allow you to parse certain path.
     *
     * @param $document
     * @param null $xpathStart
     * @param null $modelPath
     * @param null $modelNs
     * @return array|\SimpleXMLElement|\SimpleXMLElement[]
     */
    public function parse($document, $xpathStart = null, $modelPath = null, $modelNs = null)
    {
        $simpleXml = simplexml_load_string($document);

        if ($xpathStart) {
            $simpleXml = $simpleXml->xpath($xpathStart);

        }
        if ($modelPath && $modelNs) {
            return $this->convertToModels($simpleXml, $modelNs, $modelPath);
        }
        return $simpleXml;
    }

    /**
     * Convert simple XML element into models.
     * Inject data from simple xml into model, if found.
     * @todo - check models existence.
     *
     * @param $simpleXml
     * @param null $modelNs
     * @param null $modelPath
     * @return array
     */
    protected function convertToModels($simpleXml, $modelNs = null, $modelPath = null)
    {
        $structure = $this->scanStructure($modelPath);

        $objects = [];
        $current = null;

        foreach($simpleXml as $_i => $result)
        {

            foreach($result as $elementName => $data)
            {
                $className = $modelNs . '\\' . $elementName;

                if (!class_exists($className)) {
                    continue;
                }
                $classInstance = new $className;

                if (!($classInstance instanceof SimpleXmlDataInjectorInterface)) {
                    continue;
                }

                $classInstance->injectData($data);
                $objects[] = $classInstance;
            }
        }
        return $objects;
    }



    /**
     * Read modules structure.
     *
     * @param string $modelPath
     * @return array
     */
    protected function scanStructure($modelPath = '')
    {
        $structure = [];

        $dir = dir($modelPath);

        while($file = $dir->read())
        {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            $fileAbsolute = $modelPath . $file;
            if (is_dir($fileAbsolute)) {
                $structure[$file] = [];
                $structure[$file] = array_merge($structure[$file], $this->scanStructure($fileAbsolute . DIRECTORY_SEPARATOR));

            } else {
                $structure['classes'] = substr($file, 0, strrpos($file, '.'));

            }

        }

        return $structure;
    }
}
