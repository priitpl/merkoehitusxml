<?php

namespace lib\Controller\Actions;

/**
 * Created by PhpStorm.
 * User: priitpl
 */
abstract class AbstractAction
{
    abstract public function process();
}
