<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Reader;
use lib\Reader\Exceptions\FileNotExistsException;


/**
 * Simple file reader.
 * Read local file, if it exists.
 *
 * @package lib\Reader
 */
class FileReader implements ReaderInterface
{

    protected $filePath;


    /**
     * @param $filePath
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Get local file content.
     *
     * @return string
     * @throws FileNotExistsException
     */
    public function read()
    {
        if (!file_exists($this->filePath)) {
            throw new FileNotExistsException();
        }

        $data = file_get_contents($this->filePath);

        return $data;
    }
}
