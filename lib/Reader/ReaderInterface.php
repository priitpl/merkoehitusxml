<?php

namespace lib\Reader;

/**
 * Created by PhpStorm.
 * User: priitpl
 */


/**
 * Interface ReaderInterface
 * @package lib\Reader
 */
interface ReaderInterface
{
    /**
     * Get content.
     *
     * @return string
     */
    public function read();
}
