<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Reader\HttpContentReader;


use lib\Http\Request;

abstract class AbstractHttpContentReader
{

    protected $request;

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }



}
