<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Reader\HttpContentReader;

use lib\Reader\Exceptions\BadRequestHostException;
use lib\Reader\Exceptions\RequestException;


/**
 * Simple Curl base remote/web file fetcher.
 * This implementation do not/perform any check on resource
 * and internet connection problems.
 *
 * Class Curl
 * @package lib\Reader\HttpContentReader
 */
class Curl extends AbstractHttpContentReader
{
    protected $options = [];


    public function __construct($options = [])
    {
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        /** @var  $curl \CURLFile*/
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->getRequest()->getUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->getRequest()->getTimeout());

        foreach($this->options as $opt => $val)
        {
            curl_setopt($curl, $opt, $val);
        }

        $data = curl_exec($curl);

        // throw exception on curl error
        if ($data === false) {

            switch(curl_errno($curl)){
                case CURLE_COULDNT_RESOLVE_HOST:
                    throw new BadRequestHostException;
            }

            throw new RequestException(curl_error($curl), curl_errno($curl));
        }
        curl_close($curl);

        return $data;
    }
}
