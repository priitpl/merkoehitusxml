<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Reader;
use lib\Http\Request;
use lib\Reader\HttpContentReader\AbstractHttpContentReader;
use lib\Reader\HttpContentReader\HttpContentReaderInterface;


/**
 * Class HttpReader.
 *
 *
 * @package lib\Reader
 */
class HttpReader implements ReaderInterface
{

    /**
     * @var string
     */
    protected $url;

    /**
     * @var HttpContentReaderInterface
     */
    protected $contentReader;


    /**
     * @param AbstractHttpContentReader $httpContentReader
     * @param $url
     */
    public function __construct(AbstractHttpContentReader $httpContentReader, Request $request)
    {
        $this->contentReader = $httpContentReader;
        $this->contentReader->setRequest($request);
    }

    /**
     * @inheritdoc
     */
    public function read()
    {
        return $this->contentReader->getContent();
    }


}
