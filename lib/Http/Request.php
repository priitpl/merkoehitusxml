<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace lib\Http;


/**
 * Class Request.
 *
 * Simple Request object, stores url, request query and action method to perform.
 *
 * @package lib\Http
 */
class Request
{

    const GET = 'GET';

    const POST = 'POST';

    /**
     * Default request timeout.
     * In seconds
     */
    const DEFAULT_TIMEOUT = 5;


    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $method = self::GET;

    /**
     * @var array
     */
    protected $queue = [];

    /**
     * @var int
     */
    protected $timeout = self::DEFAULT_TIMEOUT;


    /**
     * @param $url
     * @param string $method
     * @param array $queue
     */
    public function __construct($url, $method = self::GET, $queue = [], $timeout = self::DEFAULT_TIMEOUT)
    {
        $this->url = $url;

        if ($method && in_array($method, array(self::GET, self::POST))) {
            $this->method = $method;
        }

        $this->queue = $queue;

        $this->timeout = $timeout = (int)$timeout >= 0 ? $timeout : self::DEFAULT_TIMEOUT;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param array $queue
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }
}
