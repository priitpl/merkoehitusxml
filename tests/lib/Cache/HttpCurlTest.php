<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace tests\lib\Cache;


use lib\Http\Request;
use lib\Reader\HttpContentReader\Curl;
use lib\Reader\HttpReader;

class HttpCurlTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException lib\Reader\Exceptions\BadRequestHostException
     */
    public function testBadHostRequestException()
    {
        $request = new Request('asdlkasjd');

        $httpReader = new HttpReader(new Curl(), $request);
        $httpReader->read();
    }

}
