<?php

/**
 * Created by PhpStorm.
 * User: priitpl
 */

namespace tests\lib\Cache;


use lib\Cache\Exceptions\ForbiddenCachePathException;
use lib\Cache\FileCache;

class FileCacheTest extends \PHPUnit_Framework_TestCase
{


    /**
     * @expectedException lib\Cache\Exceptions\ForbiddenCachePathException
     *
     * @throws \lib\Cache\Exceptions\BadDirectoryNameException
     * @throws \lib\Cache\Exceptions\DirectoryNotWritableException
     * @throws \lib\Cache\Exceptions\FileNotExistsException
     * @throws \lib\Cache\Exceptions\ForbiddenCachePathException
     * @throws \lib\Cache\Exceptions\NotADirectoryException
     */
    public function testForbiddenCacheDirCreation()
    {
        $fileCache = new FileCache();
        $fileCache->setCacheDir('/user/priitpl/cache/../cache/../tmp');
    }


    /**
     * Test file sanitization process.
     */
    public function testFilePathSanitization()
    {
        $fileCache = new FileCache();

        // Paths to check
        $pathsToCheck = array(
            '/tmp/cache' => '/tmp/cache',
            '/tmp/cache/../cache' => '/tmp/cache',
            '/tmp/cache/username/../../' => '/tmp',
            '/tmp/../../' => false,
            '/tmp/././//' => '/tmp'
        );

        // Process each directory and check for valid result
        foreach($pathsToCheck as $path => $correctPath)
        {
            $this->assertEquals($correctPath, $fileCache->sanitizePath($path));
        }
    }


    /**
     * Test new folder creation
     */
    public function testNotExistedDirectoryCreation()
    {
        $fileCache = new FileCache();

        $randomDir = rand();
        $cachePath = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $randomDir;

        $fileCache->setCacheDir($cachePath);

        $this->assertEquals(true, file_exists($cachePath));
        $this->assertEquals(true, is_dir($cachePath));

        rmdir($cachePath);
    }

    /**
     * Test, if created folder is writable.
     */
    public function testNewCacheFolderWritingPermissions()
    {
        $fileCache = new FileCache();

        $randomDir = rand();
        $cachePath = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $randomDir;
        $fileCache->setCacheDir($cachePath);

        $this->assertEquals(true, is_writable($cachePath));

        rmdir($cachePath);
    }


}
