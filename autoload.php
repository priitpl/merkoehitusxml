<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */


function __autoload($class)
{
    if (strpos($class, 'Twig') === 0) {
        return __twigAutload($class);
    }

    $parts = explode('\\', $class);
    $classFile = end($parts) . '.php';
    array_pop($parts);

    $fullPath = join(DIRECTORY_SEPARATOR, $parts) . DIRECTORY_SEPARATOR . $classFile;

    if (!file_exists($fullPath)) {
    }

    require join(DIRECTORY_SEPARATOR, $parts) . DIRECTORY_SEPARATOR . $classFile;
}

function __twigAutload($class)
{
    $classParts = array_merge([__DIR__, 'vendor', 'twig', 'twig', 'lib'], explode('_', $class));
    $classAbsolutePath = join(DIRECTORY_SEPARATOR, $classParts);
    $classFilePath = $classAbsolutePath . '.php';

    if (file_exists($classFilePath)) {
        require_once($classFilePath);
    }
}


spl_autoload_register('__autoload');
