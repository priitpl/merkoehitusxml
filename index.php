<?php

$loadComposerAutoload = false;

if ($loadComposerAutoload && file_exists('vendor/autoload.php')) {
    $loader = require 'vendor/autoload.php';
    $loader->add('lib', __DIR__);

} else {
    require_once('autoload.php');

}

$useTwig = false;

if (class_exists('Twig_Loader_Filesystem')){
    $loader = new Twig_Loader_Filesystem(__DIR__ . DIRECTORY_SEPARATOR . 'templates');
    $twig = new Twig_Environment($loader, array(
        'cache' => __DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'twig'
    ));
    $useTwig = true;
}




$merkoEhitusService = new \lib\MerkoEhitusInfo();

/**
 * @var $indexs \lib\Model\TseMessage\Indices\Index[]
 */
$indexes = $merkoEhitusService->getIndexes();

/** @var  $orders \lib\Model\TseMessage\Instrument\Orders\Order [] */
$orders  = $merkoEhitusService->getOrders();


// If Twig is isntalled, then use twig as template engine
if ($useTwig) {
    echo $twig->render('index.html', array(
        'indexes'   => $indexes,
        'orders'    => $orders
    ));
    
}
// Otherwise use simple PHP templating.
else {

    try{
        ob_start();
        include_once('templates/index.php');
        $content = ob_get_contents();
        ob_end_clean();
        echo $content;

    } catch (Exception $e) {
        echo "Something went wrong";

    }
}
