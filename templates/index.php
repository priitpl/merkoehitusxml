<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 */
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zave test</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="styles/styles.css">
</head>
<?php throw new Exception("hren") ;?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <?php if (isset($orders)): ?>
                <h2>Latest orders</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <?php
                    /** @var  $order \lib\Model\TseMessage\Instruments\Orders\Order */
                    foreach($orders as $order):
                    ?>
                    <tr>
                        <td><?php echo $order->getQuantity() ?></td>
                        <td><?php echo $order->getPrice() ?></td>
                        <td><?php if ($order->getTime()) { echo $order->getTime()->format('m.d.Y H:i:s'); } ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php else: ?>
                Orders not set
                <?php endif; ?>
            </div>

            <div class="col-md-5">
                <?php if (isset($indexes)): ?>
                <h2>Indexes</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Last value</th>
                        <th>Change</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <?php
                    /** @var  $index \lib\Model\TseMessage\Indices\Index */
                    foreach($indexes as $index):
                    $isUp = $index->getPercentageChange() > 0;
                    $isDown = $index->getPercentageChange() < 0;
                    ?>
                    <tr>
                        <td><?php echo $index->getFullName() ?></td>
                        <td><?php echo $index->getLastValue() ?></td>
                        <td class="<?php if ($isUp):?>changes-up<?php elseif ($isDown):?>changes-down<?php else :?>no-changes<?php endif;?>">
                            <?php if ($isUp):?><i class="glyphicon glyphicon-arrow-up"></i><?php endif;?>
                            <?php if ($isDown):?><i class="glyphicon glyphicon-arrow-down"></i><?php endif;?>
                            <?php echo $index->getPercentageChange()?>
                        </td>
                        <td><?php if ($index->getLastValueTime()){ echo $index->getLastValueTime()->format('m.d.Y H:i:s'); }?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php else : ?>
                Indexes not set
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
</html>